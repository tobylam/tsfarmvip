#!/bin/bash
#
echo -e "\033[36m'**************  Provisioning virtual machine  **************'\033[0m"

echo -e "\033[36m'**************  Configuring to use local repos  **************'\033[0m"
sudo rm -fr /etc/yum.repos.d
sudo ln -s /vagrant/etc/yum.repos.d/ /etc/yum.repos.d
sudo rm -f /vagrant/etc/yum.repos.d/Centos-*

echo -e "\033[36m'**************  Updating all packages  **************'\033[0m"
yum update -y

echo -e "\033[36m'**************  Disabling conflicting services  **************'\033[0m"
sudo chkconfig postfix off
sudo service postfix stop

echo -e "\033[36m'**************  Installing GNF Root Certificates  **************'\033[0m"
sudo yum install ca-certificates -y
sudo update-ca-trust force-enable
sudo rm -fr /etc/pki/ca-trust/source/anchors
sudo ln -s /vagrant/etc/pki/ca-trust/source/anchors/ /etc/pki/ca-trust/source/anchors
sudo update-ca-trust extract

echo -e "\033[36m'**************  Installing NGINX  **************'\033[0m"
sudo yum install -y nginx
sudo rm /etc/nginx/nginx.conf
sudo ln -s /vagrant/etc/nginx/nginx.conf /etc/nginx/nginx.conf
sudo rm -fr /etc/nginx/conf.d
sudo ln -s /vagrant/etc/nginx/conf.d/ /etc/nginx/conf.d
sudo ln -s /vagrant/etc/nginx/ssl/ /etc/nginx/ssl
sudo chkconfig nginx on
sudo service nginx configtest && sudo service nginx start

echo -e "\033[36m'**************  Permitting approved traffic  **************'\033[0m"
sudo iptables -I INPUT 5 -p tcp --dport 25 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 6 -p tcp --dport 135 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 7 -p tcp --dport 143 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 8 -p tcp --dport 389 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 9 -p tcp --dport 443 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 10 -p tcp --dport 445 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 11 -p tcp --dport 448 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 12 -p tcp --dport 636 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 13 -p tcp --dport 993 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 14 -p tcp --dport 995 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 15 -p tcp --dport 3389 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 16 -p tcp --dport 5061 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 17 -p tcp --dport 6001 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 18 -p tcp --dport 9442 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 19 -p tcp --dport 49152 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 20 -p tcp --dport 55000 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 21 -p tcp --dport 55001 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 22 -p tcp --dport 59532 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 23 -p tcp --dport 59533 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 24 -p udp --dport 68 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 25 -p udp --dport 8057 -m state --state NEW -j ACCEPT
sudo iptables -I INPUT 26 -p udp --dport 49152 -m state --state NEW -j ACCEPT
sudo service iptables save

echo -e "\033[36m'**************  Listing iptables firewall rules  **************'\033[0m"
sudo iptables -L -v -n
